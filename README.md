# Important guidelines #

### Build configuration ###

Please open solution menu "Configuration Manager"
![congManager.PNG](https://bitbucket.org/repo/5r46p8/images/2618273429-congManager.PNG)

Dependency resolution projects should be unchecked as you don't want to build them twice.

### Host project dependencies ###

Every host project that uses onions integration should set project dependencies (Use "Project Dependencies" on solution):
![projDep.PNG](https://bitbucket.org/repo/5r46p8/images/445178776-projDep.PNG)

You have to select all real dependencies (NOT ONLY CORE) but implementations as well in order to build them first - this is very importatnt!