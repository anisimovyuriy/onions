﻿using Onion.Core.Services.DependencyResolution;
using SimpleInjector;

namespace Onion.Infrastructure.DependencyResolver.Services
{
    public class SimpleInjectorScope : IScope
    {
        private readonly Scope _scope;

        public SimpleInjectorScope(Scope scope)
        {
            _scope = scope;
        }

        public void Dispose()
        {
            _scope.Dispose();
        }
    }
}