﻿using System.Collections.Generic;
using Onion.Core.Services.DependencyResolution;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace Onion.Infrastructure.DependencyResolver.Services
{
    public class SimpleInjectorContainer : IContainer
    {
        private readonly Container _container;

        public SimpleInjectorContainer(Container container)
        {
            _container = container;
        }

        public IScope BeginExecutionContextScope()
        {
            return new SimpleInjectorScope(_container.BeginExecutionContextScope());
        }

        public TInterface Resolve<TInterface>() where TInterface : class
        {
            return _container.GetInstance<TInterface>();
        }

        public IEnumerable<TInterface> ResolveAll<TInterface>() where TInterface : class
        {
            return _container.GetAllInstances<TInterface>();
        }
    }
}