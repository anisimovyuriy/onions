﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Onion.Api.Startup))]

namespace Onion.Api
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
