﻿using System;
using System.Linq;
using Onion.Core.DomainServices.Product;
using Onion.Core.ViewModels.Product;

namespace Onion.Infrastructure.DomainServices.Product
{
    public class ProductDomainService : IProductDomainService
    {
        public IQueryable<ProductViewModel> GetProducts()
        {
            throw new NotImplementedException();
        }

        public ProductViewModel GetProductById(Guid id)
        {
            throw new NotImplementedException();
        }

        public ProductViewModel Add(ProductViewModel product)
        {
            throw new NotImplementedException();
        }

        public void Remove(ProductViewModel product)
        {
            throw new NotImplementedException();
        }

        public ProductViewModel Update(ProductViewModel product)
        {
            throw new NotImplementedException();
        }
    }
}
