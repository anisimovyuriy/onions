﻿using System;

namespace Onion.Infrastructure.Services.Cache
{
    public abstract class BaseCacheService
    {
        public virtual T Get<T>(string key)
        {
            return default(T);
        }

        public virtual void Add<T>(string key, T value)
        {
            throw new NotImplementedException();
        }

        public virtual void Set<T>(string key, T value)
        {
            throw new NotImplementedException();
        }

        public virtual void Remove(string key)
        {
            throw new NotImplementedException();
        }
    }
}
