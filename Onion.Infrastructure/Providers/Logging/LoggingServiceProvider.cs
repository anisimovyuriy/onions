﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Onion.Core.Data.Enums.Logging;
using Onion.Core.ServiceProviders.Logging;
using Onion.Core.Services.Logging;

namespace Onion.Infrastructure.Providers.Logging
{
    public class LoggingServiceProvider : ILoggingServiceProvider
    {
        public LoggingServiceProvider(IEnumerable<ILoggingService> services)
        {
            Services = services;
        }

        private IEnumerable<ILoggingService> _backupServices;

        #region ILoggingServiceProvider implementation

        public IEnumerable<ILoggingService> Services { get; set; }

        public void Debug(string msg)
        {
            Log(LoggingLevels.Debug, msg);
        }

        public void Info(string msg)
        {
            Log(LoggingLevels.Info, msg);
        }

        public void Warn(string msg)
        {
            Log(LoggingLevels.Warn, msg);
        }

        public void Error(string msg, Exception exception = null)
        {
            Log(LoggingLevels.Error, msg, exception);
        }

        #endregion

        private void Log(LoggingLevels level, string msg, Exception exception = null)
        {
            var actualServices = Services.Where(s => s.Level <= level);
            foreach (var service in actualServices)
            {
                LogMessage(service, msg, level);
            }
        }

        private void LogMessage(ILoggingService service, string msg, LoggingLevels level, Exception exception = null,
            bool backup = false)
        {
            Action logAction = () =>
            {
                LoggingInternals(service, msg, level, backup);
            };

            if (service.RunInBackground)
            {
                Task.Factory.StartNew(logAction);
            }
            else
            {
                logAction();
            }
        }

        private void LoggingInternals(ILoggingService service, string msg, LoggingLevels level, bool backup)
        {
            try
            {
                service.Log(msg, level);
            }
            catch (Exception ex)
            {
                if (!backup)
                {
                    _backupServices = Services.Where(s => s.Backup);
                    foreach (var backupService in _backupServices)
                    {
                        LogMessage(backupService, string.Format("Unable to log report to {0}", service.GetType().Name),
                            LoggingLevels.Error, ex, backup: true);
                    }
                }
            }
        }
    }
}
