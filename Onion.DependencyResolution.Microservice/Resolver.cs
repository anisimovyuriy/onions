﻿using Onion.Core.DependencyManager;
using Onion.Core.DomainServices.Product;
using Onion.Core.ServiceProviders.Logging;
using Onion.Core.Services.Cache;
using Onion.Core.Services.Logging;
using Onion.Infrastructure.Cache.Memory.Services;
using Onion.Infrastructure.DependencyResolver.Services;
using Onion.Infrastructure.DomainServices.Product;
using Onion.Infrastructure.Logging.Console.Services;
using Onion.Infrastructure.Logging.ElasticSearch.Services;
using Onion.Infrastructure.Logging.FileSystem.Services;
using Onion.Infrastructure.Providers.Logging;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;

namespace Onion.DependencyResolution.Microservice
{
    public static class Resolver
    {
        public static void RegisterTypes()
        {
            var container = new Container();
            ServiceLocator.Instance.ContainerInjectFunc = () => new SimpleInjectorContainer(container);
            container.Options.DefaultScopedLifestyle = new ExecutionContextScopeLifestyle();

            container.Register<IProductDomainService, ProductDomainService>();
            container.Register<ICacheService, MemoryCacheService>();
            container.Register<ILoggingServiceProvider, LoggingServiceProvider>(Lifestyle.Singleton);

            container.RegisterCollection<ILoggingService>(new[]
            {
                typeof (ElasticLoggingService),
                typeof (FileLoggingService),
                typeof (ConsoleLoggingService),
            }
                );
        }
    }
}
