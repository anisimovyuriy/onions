﻿namespace Onion.Core.Data.Enums.Logging
{
    public enum LoggingLevels
    {
        Debug = 0,
        Info = 1,
        Warn = 2,
        Error = 3
    }
}
