﻿using System;
using System.Linq;
using Onion.Core.DependencyManager;
using Onion.Core.ServiceProviders.Logging;

namespace Onion.Microservice
{
    class Program
    {
        static void Main(string[] args)
        {
            // dependency resolution happens here
            ResolverLoader.Load();

            // resolving interface
            var loggingServiceProvider = ServiceLocator.Instance.Container.Resolve<ILoggingServiceProvider>();

            Console.WriteLine("ILoggingServiceProvider resolved to {0}", loggingServiceProvider.GetType().FullName);
            Console.WriteLine("ILoggingServiceProvider.Services.Count = {0}", loggingServiceProvider.Services.Count());
            foreach (var loggingService in loggingServiceProvider.Services)
            {
                Console.WriteLine("ILoggingService to {0}", loggingService.GetType().FullName);
            }

            Console.ReadLine();
        }
    }
}
