﻿using System;
using System.Linq;
using Onion.Core.ViewModels.Product;

namespace Onion.Core.DomainServices.Product
{
    public interface IProductDomainService
    {
        IQueryable<ProductViewModel> GetProducts();
        ProductViewModel GetProductById(Guid id);
        ProductViewModel Add(ProductViewModel product);
        void Remove(ProductViewModel product);
        ProductViewModel Update(ProductViewModel product);
    }
}
