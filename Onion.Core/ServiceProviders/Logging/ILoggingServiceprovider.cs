﻿using System;
using System.Collections.Generic;
using Onion.Core.Services.Logging;

namespace Onion.Core.ServiceProviders.Logging
{
    public interface ILoggingServiceProvider
    {
        IEnumerable<ILoggingService> Services { set; get; }
        void Debug(string msg);
        void Info(string msg);
        void Warn(string msg);
        void Error(string msg, Exception exception = null);
    }
}
