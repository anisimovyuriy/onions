﻿using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Onion.Core.DependencyManager
{
    /// <summary>
    /// Loads the appropriate dependency resolvers based on the config file of the application.
    /// </summary>
    public static class ResolverLoader
    {
        private const string Resolvertype = "ResolverType";
        private const string Resolver = "Resolver";
        private const string Registertypes = "RegisterTypes";

        /// <summary>
        /// Load the application's instance of the dependency resolver and regsiter it's types in the container.
        /// </summary>
        public static void Load()
        {
            Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

            var resolverAssemblyName = ConfigurationManager.AppSettings[Resolvertype];

            if (!string.IsNullOrEmpty(resolverAssemblyName))
            {
                var dependencyDll = new FileInfo(resolverAssemblyName);
                var dependencyAssembly = Assembly.LoadFile(dependencyDll.FullName);
                var dependencyAssemblyType = dependencyAssembly.ExportedTypes.Single(t => t.Name == Resolver);
                var registerTypesMethod = dependencyAssemblyType.GetMethod(Registertypes, BindingFlags.Public | BindingFlags.Static);

                registerTypesMethod.Invoke(null, null);
            }
        }
    }
}
