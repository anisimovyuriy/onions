﻿using System;
using Onion.Core.Services.DependencyResolution;

namespace Onion.Core.DependencyManager
{
    /// <summary>
    ///     The ServiceLocator class provides an API around the IoC container.
    /// </summary>
    public sealed class ServiceLocator
    {
        #region Private Members
        private static class SingletonInstantiator
        {
            internal static readonly ServiceLocator Instance = new ServiceLocator();

            static SingletonInstantiator()
            {
            }
        }

        private Func<IContainer> _containerInjectFunc;
        #endregion

        #region Public Properties
        /// <summary>
        ///     Gets the dependency container.
        /// </summary>
        public IContainer Container { get; private set; }

        /// <summary>
        ///     Gets the dependency container.
        /// </summary>
        public Func<IContainer> ContainerInjectFunc
        {
            get { return _containerInjectFunc; }
            set
            {
                _containerInjectFunc = value;
                Container = ContainerInjectFunc();
            }
        }

        /// <summary>
        ///     Static. Returns the singleton instance of the ServiceLocator
        /// </summary>
        public static ServiceLocator Instance {get { return SingletonInstantiator.Instance; }}

        #endregion
    }
}