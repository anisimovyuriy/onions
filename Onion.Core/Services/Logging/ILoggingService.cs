﻿using System;
using Onion.Core.Data.Enums.Logging;

namespace Onion.Core.Services.Logging
{
    public interface ILoggingService
    {
        void Log(string msg, LoggingLevels level, Exception exception = null);
        LoggingLevels Level { get; }
        bool RunInBackground { get; }
        bool Backup { get; }
    }
}
