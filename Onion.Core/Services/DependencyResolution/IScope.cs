﻿using System;

namespace Onion.Core.Services.DependencyResolution
{
    public interface IScope : IDisposable
    {
    }
}
