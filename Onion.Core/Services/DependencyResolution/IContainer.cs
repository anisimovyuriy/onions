﻿using System.Collections.Generic;

namespace Onion.Core.Services.DependencyResolution
{
    public interface IContainer
    {
        IScope BeginExecutionContextScope();

        TInterface Resolve<TInterface>() where TInterface : class;

        //object Resolve(Type interfaceType);

        IEnumerable<TInterface> ResolveAll<TInterface>() where TInterface : class;

        //void Verify();
    }
}