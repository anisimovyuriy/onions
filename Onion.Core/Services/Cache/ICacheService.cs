﻿namespace Onion.Core.Services.Cache
{
    public interface ICacheService
    {
        T Get<T>(string key);
        void Add<T>(string key, T value);
        void Set<T>(string key, T value);
        void Remove(string key);
    }
}
