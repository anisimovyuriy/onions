﻿using System;
using Onion.Core.Services.Cache;
using Onion.Infrastructure.Services.Cache;

namespace Onion.Infrastructure.Cache.Redis.Services
{
    public class RedisCacheService : BaseCacheService, ICacheService
    {
        public override void Add<T>(string key, T value)
        {
            base.Add(key, value);
            throw new NotImplementedException();
        }

        public override void Set<T>(string key, T value)
        {
            base.Set(key, value);
            throw new NotImplementedException();
        }

        public override void Remove(string key)
        {
            base.Remove(key);
            throw new NotImplementedException();
        }
    }
}
