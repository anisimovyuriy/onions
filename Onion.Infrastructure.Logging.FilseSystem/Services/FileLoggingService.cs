﻿using System;
using Onion.Core.Data.Enums.Logging;
using Onion.Core.Services.Logging;
using Onion.Infrastructure.Services.Logging;

namespace Onion.Infrastructure.Logging.FileSystem.Services
{
    public class FileLoggingService : BaseLoggingService, ILoggingService
    {
        public void Log(string msg, LoggingLevels level, Exception exception = null)
        {
            throw new NotImplementedException();
        }

        public LoggingLevels Level
        {
            get { throw new NotImplementedException(); }
        }

        public bool RunInBackground
        {
            get { throw new NotImplementedException(); }
        }

        public bool Backup
        {
            get { throw new NotImplementedException(); }
        }
    }
}
